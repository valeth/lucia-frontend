interface ProjectImages {
  [project: string]: {
    full: string,
    white: string,
    icon: string
  }
}

interface Avatars {
  [user: string]: string
}

interface Images {
  core: {
    [index: string]: string
  }
  avatars: Avatars,
  projects: ProjectImages
}

const projectImages: ProjectImages = {
  sigma: {
    full: '/img/project_sigma_colorful.png',
    white: '/img/project_sigma_white.png',
    icon: '/img/project_sigma_icon_white.png'
  },
  javelin: {
    full: '/img/project_javelin_colorful.png',
    white: '/img/project_javelin_white.png',
    icon: '/img/project_javelin_icon.png'
  },
  kyanite: {
    full: '/img/project_kyanite_colorful.png',
    white: '/img/project_kyanite_white.png',
    icon: '/img/project_kyanite_icon.png'
  },
  wkBreeze: {
    full: '/img/project_wk_breeze_colorful.png',
    white: '/img/project_wk_breeze_white.png',
    icon: '/img/project_wk_breeze_icon.png'
  },
  snoo: {
    full: '/img/project_snoo_colorful.png',
    white: '/img/project_snoo_colorful.png',
    icon: '/img/project_snoo_colorful.png'
  }
}

const avatars: Avatars = {
  alex: '/img/alex.gif',
  valeth: '/img/valeth.png',
  awakening: '/img/awakening.png',
  shifty: '/img/shifty.gif'
}

const images: Images = {
  core: {
    lcWhite: '/img/lc_white.png',
    lcWhiteCircle: '/img/lc_white_circle.png',
    lcWhiteCircleGlitch: '/img/lc_white_circle_glitch.png',
    lcNotFoundFox: '/img/lc_not_found_fox.svg',
    lcUnknownUser: '/img/unknown_user.png',
    lcPoster: '/img/lc_poster.png'
  },
  avatars: avatars,
  projects: projectImages
}

export default images
