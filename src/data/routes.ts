import { RouteConfig } from 'vue-router'

import meta from '@/data/meta'

// Lucia
import LCLanding from '@/pages/lucia/Landing.vue'
import LCProjects from '@/pages/lucia/Projects.vue'
import LCAbout from '@/pages/lucia/About.vue'
import LCDonate from '@/pages/lucia/Donate.vue'

// Livestream
import LCLivestream from '@/pages/lucia/Livestream.vue'
import LCLivestreamHub from '@/pages/lucia/LivestreamHub.vue'

// Sigma
import SigmaLanding from '@/pages/sigma/Landing.vue'
import SigmaAbout from '@/pages/sigma/About.vue'
import SigmaStats from '@/pages/sigma/Stats.vue'
import SigmaCommands from '@/pages/sigma/Commands.vue'
import SigmaLeaderboards from '@/pages/sigma/Leaderboards.vue'

// Errors
import LCNotFound from '@/pages/lucia/NotFound.vue'

const routes: RouteConfig[] = [
  // Lucia
  { path: '/', name: 'LCLanding', component: LCLanding, meta: meta.lc.landing },
  { path: '/projects', name: 'LCProjects', component: LCProjects, meta: meta.lc.projects },
  { path: '/about', name: 'LCAbout', component: LCAbout, meta: meta.lc.about },
  { path: '/donate', name: 'LCDonate', component: LCDonate, meta: meta.lc.donate },

  // Livestreaming
  { path: '/live', name: 'LCLivestreamHub', component: LCLivestreamHub, meta: meta.lc.liveHub },
  { path: '/live/*', name: 'LCLivestream', component: LCLivestream, meta: meta.lc.live },

  // Sigma
  { path: '/(sigma)', name: 'SigmaLanding', component: SigmaLanding, meta: meta.sigma.landing },
  { path: '/(sigma)/about', name: 'SigmaAbout', component: SigmaAbout, meta: meta.sigma.about },
  { path: '/(sigma)/stats', name: 'SigmaStats', component: SigmaStats, meta: meta.sigma.stats },
  { path: '/(sigma)/commands', name: 'SigmaCommands', component: SigmaCommands, meta: meta.sigma.commands },
  { path: '/(sigma)/leaderboards', name: 'SigmaLeaderboards', component: SigmaLeaderboards, meta: meta.sigma.leaderboards },

  // Errors
  { path: '/404', name: 'LCNotFound', component: LCNotFound, meta: meta.lc.notFound },
  { path: '/*', name: 'LCNotFoundRedirect', redirect: '/404', meta: meta.lc.landing }
]

export default routes
