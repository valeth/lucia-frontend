import icons from '@/data/icons'

export type ColorArray = string[]

interface Colors {
  [index: string]: ColorArray
}

const colors: Colors = {
  lucia: ['#190d3f', '#29166a', '#4412a1', '#60136c'],
  sigma: ['#051512', '#0a2923', '#0f3e35', '#145247'],
  javelin: ['#95375b', '#552a52', '#363168', '#231a33']
}

const meta = {
  lc: {
    landing: { title: 'Lucia\'s Cipher - Home', icon: icons.lucia, colors: colors.lucia },
    projects: { title: 'Lucia\'s Cipher - Project', icon: icons.lucia, colors: colors.lucia },
    about: { title: 'Lucia\'s Cipher - About', icon: icons.lucia, colors: colors.lucia },
    donate: { title: 'Lucia\'s Cipher - Donate', icon: icons.lucia, colors: colors.lucia },
    notFound: { title: 'Lucia\'s Cipher - Page Not Found', icon: icons.lucia, colors: colors.lucia },
    liveHub: { title: 'Lucia\'s Cipher - Livestream Hub', icon: icons.javelin, colors: colors.javelin },
    live: { title: 'Lucia\'s Cipher - Livestream', icon: icons.javelin, colors: colors.javelin }
  },
  sigma: {
    landing: { title: 'Apex Sigma - Home', icon: icons.sigma, colors: colors.sigma },
    about: { title: 'Apex Sigma - About', icon: icons.sigma, colors: colors.sigma },
    stats: { title: 'Apex Sigma - Stats', icon: icons.sigma, colors: colors.sigma },
    commands: { title: 'Apex Sigma - Commands', icon: icons.sigma, colors: colors.sigma },
    leaderboards: { title: 'Apex Sigma - Leaderboards', icon: icons.sigma, colors: colors.sigma }
  }
}

export default meta
