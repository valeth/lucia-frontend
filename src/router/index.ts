import Vue from 'vue'
import Router from 'vue-router'
import URLRoutes from '@/data/routes'

Vue.use(Router)
export default new Router({
  mode: 'history',
  routes: URLRoutes
})
