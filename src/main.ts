import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './utils/filters'
import { ColorArray } from '@/data/meta'
import VueAnalytics from 'vue-analytics'

Vue.config.productionTip = false

function paintBackground (colorArray: ColorArray) {
  const htmlSel = document.querySelector('html')
  if (!htmlSel) return
  htmlSel.style.backgroundImage = 'linear-gradient(160deg, ' + colorArray.join(', ') + ' 75%)'

  const colorSel = document.getElementById('meta-theme-color')
  if (!colorSel) return
  colorSel.setAttribute('content', colorArray[0])
}

function switchIcon (newIcon: string) {
  if (!newIcon) return
  const location = window.location.href.split('/').slice(0, 3).join('/')
  const iconElements = [
    'meta-icon', 'meta-shortcut-icon', 'meta-twitter-image',
    'meta-og-image', 'meta-og-image-secure'
  ]
  iconElements.forEach((elementId: string) => {
    const toChange = document.getElementById(elementId)
    if (toChange) {
      const fullIconLocation = location + newIcon
      if (toChange.getAttribute('content')) toChange.setAttribute('content', fullIconLocation)
      if (toChange.getAttribute('href')) toChange.setAttribute('href', fullIconLocation)
    }
  })
}

router.afterEach((target, _origin) => {
  document.title = target.meta.title
  paintBackground(target.meta.colors)
  switchIcon(target.meta.icon)
})

Vue.use(VueAnalytics, {
  id: 'UA-140292420-1',
  router
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
