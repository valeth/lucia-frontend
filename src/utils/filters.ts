import Vue from 'vue'

Vue.filter('upper', (value: string) => value.toUpperCase())

Vue.filter('title', (value: string) => {
  return [value[0].toUpperCase(), value.slice(1, value.length).toLowerCase()].join('')
})

Vue.filter('hhmmss', function (secondsTotal: number) {
  secondsTotal = Math.round(secondsTotal) // round in case we got a float number

  const hours = Math.floor(secondsTotal / 3600)
  const minutes = Math.floor((secondsTotal % 3600) / 60)
  const seconds = (secondsTotal % 60)

  return [hours, minutes, seconds]
    .map((n: number) => n.toString().padStart(2, '0'))
    .join(':')
})
